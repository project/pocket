<?php

namespace Drupal\pocket_user\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\pocket\AccessToken;
use Drupal\pocket\Client\PocketClientFactoryInterface;
use Drupal\pocket_user\PocketUserManager;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function assert;

/**
 * Form for associating a Drupal account with a Pocket account.
 *
 * @package Drupal\pocket_user\Form
 */
class PocketUserForm extends FormBase {

  /**
   * The Pocket client factory service.
   *
   * @var \Drupal\pocket\Client\PocketClientFactoryInterface
   */
  protected PocketClientFactoryInterface $clientFactory;

  /**
   * The Pocket user manager service.
   *
   * @var \Drupal\pocket_user\PocketUserManager
   */
  protected PocketUserManager $manager;

  /**
   * PocketUserForm constructor.
   *
   * @param \Drupal\pocket\Client\PocketClientFactoryInterface $clientFactory
   *   The Pocket client factory service.
   * @param \Drupal\pocket_user\PocketUserManager $manager
   *   The Pocket user manager service.
   */
  public function __construct(
    PocketClientFactoryInterface $clientFactory,
    PocketUserManager $manager
  ) {
    $this->clientFactory = $clientFactory;
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('pocket.client'),
      $container->get('pocket_user.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'user_pocket_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    UserInterface $user = NULL
  ): array {
    // Only add the form for an extant user account.
    if ($user instanceof UserInterface) {
      $this->buildConnectionForm($form, $user);
    }
    return $form;
  }

  /**
   * Build the connection form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\user\UserInterface $user
   *   The user account.
   */
  private function buildConnectionForm(array &$form, UserInterface $user) {
    $form['#user'] = $user;

    // If the account already has an access token saved.
    if ($access = $this->manager->getUserAccess($user->id())) {
      $form['access']['#markup'] = $this->t(
        'You have connected the Pocket account %name.',
        ['%name' => $access->getUsername()]
      );
      $form['disconnect'] = [
        '#type'        => 'submit',
        '#submit'      => ['::disconnect'],
        '#value'       => $this->t('Disconnect'),
        '#description' => $this->t(
          'You may also want to revoke access in your <a href=":url">Pocket settings</a>.',
          [
            ':url' => 'https://getpocket.com/connected_applications',
          ]
        ),
      ];
    }
    // If the account has no access, but the application has an API key.
    elseif ($this->clientFactory->hasKey()) {
      $form['connect'] = [
        '#type'   => 'submit',
        '#submit' => ['::connect'],
        '#value'  => $this->t('Connect to Pocket'),
      ];
    }
    else {
      $form['error']['#markup'] = $this->t(
        'This site does not have an API key for Pocket yet.'
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Submit handler for connecting a Pocket account.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function connect(array &$form, FormStateInterface $form_state) {
    $user = $form['#user'];
    assert($user instanceof UserInterface);

    $client = $this->clientFactory->getAuthClient();
    try {
      $url = $client->authorize([$this, 'callback'], ['user' => $user->id()]);
      $form_state->setResponse(new TrustedRedirectResponse($url->toString()));
    }
    catch (\Exception $exception) {
      $this->messenger()->addError($this->t('Failed to connect account: %message', [
        '%message' => $exception->getMessage(),
      ]));
    }
    catch (GuzzleException $exception) {
      $this->messenger()->addError($this->t('Failed to connect account: %message', [
        '%message' => $exception->getMessage(),
      ]));
    }
  }

  /**
   * Submit handler for disconnecting a Pocket account.
   *
   * @param array $form
   *   The form array.
   */
  public function disconnect(array &$form) {
    $user = $form['#user'];
    assert($user instanceof UserInterface);

    if ($this->manager->deleteUserAccess($user->id())) {
      $this->messenger()->addStatus(
        $this->t('You have disconnected your Pocket account.')
      );
    }
  }

  /**
   * Handle the callback and redirect back to the form.
   *
   * @param \Drupal\pocket\AccessToken $token
   *   The newly created access token.
   *
   * @return \Drupal\Core\Url
   *   The URL to redirect the user to.
   */
  public function callback(AccessToken $token): Url {
    $user = User::load($token->getState()['user']);
    assert($user instanceof UserInterface);
    $this->manager->setUserAccess($user->id(), $token);
    $this->messenger()->addStatus(
      $this->t(
        'You have connected your Pocket account "%user".',
        [
          '%user' => $token->getUsername(),
        ]
      )
    );
    return Url::fromRoute('pocket_user.form', ['user' => $user->id()]);
  }

}
