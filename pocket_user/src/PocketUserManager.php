<?php

namespace Drupal\pocket_user;

use Drupal\Core\Database\Connection;
use Drupal\pocket\AccessToken;

/**
 * Stores Pocket access tokens for each user.
 */
class PocketUserManager {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * PocketUserManager constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Get the access token for a specific user.
   *
   * @param string $uid
   *   The Drupal user ID.
   *
   * @return \Drupal\pocket\AccessToken|null
   *   The user's access token, if it exists.
   */
  public function getUserAccess(string $uid): ?AccessToken {
    try {
      $query = $this->database->query(
        'SELECT token, username FROM {pocket_user} WHERE uid = :uid',
        [
          ':uid' => $uid,
        ]
      );
      if ($query && ($result = $query->fetchAssoc())) {
        return new AccessToken($result['token'], $result['username']);
      }
    }
    catch (\Exception $e) {
      watchdog_exception('pocket', $e);
    }

    return NULL;
  }

  /**
   * Set the access token for a specific user.
   *
   * @param string $uid
   *   The Drupal user ID.
   * @param \Drupal\pocket\AccessToken $token
   *   The Pocket access token.
   *
   * @return bool
   *   TRUE if the token was saved.
   */
  public function setUserAccess(string $uid, AccessToken $token): bool {
    try {
      return (bool) $this->database->merge('pocket_user')
        ->key('uid', $uid)
        ->fields(
          [
            'uid'      => $uid,
            'token'    => $token->getToken(),
            'username' => $token->getUsername(),
          ]
        )
        ->execute();
    }
    catch (\Exception $e) {
      watchdog_exception('pocket', $e);
      return FALSE;
    }
  }

  /**
   * Delete a user's token.
   *
   * @param string $uid
   *   The Drupal user ID.
   *
   * @return bool
   *   TRUE if a token was deleted.
   */
  public function deleteUserAccess(string $uid): bool {
    return (bool) $this->database->delete('pocket_user')
      ->condition('uid', $uid)
      ->execute();
  }

}
