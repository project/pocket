<?php

namespace Drupal\pocket\Action;

use Drupal\pocket\PocketItemInterface;

/**
 * Defines an action executed on Pocket on behalf of a user.
 *
 * A sequence of such actions is sent to the /v3/send endpoint.
 *
 * @see https://getpocket.com/developer/docs/v3/modify
 */
interface PocketActionInterface {

  /**
   * Retrieve a parameter value.
   *
   * @param string $name
   *   The parameter name.
   *
   * @return mixed
   *   The parameter value, or NULL if it is not set.
   */
  public function get(string $name): mixed;

  /**
   * Set a parameter value.
   *
   * @param string $name
   *   The parameter name.
   * @param mixed|null $value
   *   (Optional) The parameter value, or NULL to unset the parameter.
   *
   * @return $this
   */
  public function set(string $name, mixed $value = NULL): self;

  /**
   * Convert the action parameters into an array.
   *
   * @return array
   *   Array of parameters.
   */
  public function serialize(): array;

  /**
   * Check for success of the action.
   *
   * @return bool
   *   TRUE if the action returned a successful result.
   */
  public function isSuccessful(): bool;

  /**
   * Set the action's result.
   *
   * @param bool $result
   *   The result.
   *
   * @return $this
   */
  public function setResult(bool $result): self;

  /**
   * Get the item returned from the server, if any.
   *
   * @return \Drupal\pocket\PocketItemInterface|null
   *   The result item, or NULL.
   */
  public function getResultItem() : ?PocketItemInterface;

  /**
   * Set the result item returned from the server.
   *
   * @param \Drupal\pocket\PocketItemInterface $item
   *   The result item.
   *
   * @return $this
   */
  public function setResultItem(PocketItemInterface $item): self;

}
