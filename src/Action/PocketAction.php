<?php

namespace Drupal\pocket\Action;

use Drupal\Core\Url;
use Drupal\pocket\PocketItemInterface;

/**
 * Pocket action.
 *
 * @example
 * $user_client->send([
 *   PocketAction::add(Url::fromUri('https://drupal.org/'),
 *   PocketAction::add(Node::load(42)->toUrl()),
 *   PocketAction::tag_rename('old_tag', 'new_tag'),
 * ]);
 *
 * The full list of available actions is documented here:
 *
 * @see https://getpocket.com/developer/docs/v3/modify
 */
class PocketAction implements PocketActionInterface {

  /**
   * Action parameters.
   *
   * @var array
   */
  private array $values;

  /**
   * Success status.
   *
   * @var bool
   */
  private bool $result;

  /**
   * Result item.
   *
   * @var \Drupal\pocket\PocketItemInterface
   */
  private PocketItemInterface $item;

  /**
   * PocketAction constructor.
   *
   * @param string $action
   *   The action name.
   * @param int|null $id
   *   The item ID.
   * @param array $values
   *   The action parameters.
   */
  public function __construct(string $action, int $id = NULL, array $values = []) {
    $this->values = $values;
    $this->values['action'] = $action;
    if ($id !== NULL) {
      $this->setId($id);
    }
  }

  /**
   * Add an item to Pocket.
   *
   * @param int|\Drupal\Core\Url $id_or_url
   *   Either a URL or the ID of an existing Pocket item.
   * @param array $options
   *   (Optional) Any additional parameters.
   *
   * @return static
   *
   * @see https://getpocket.com/developer/docs/v3/modify#action_add
   */
  public static function add(int|Url $id_or_url, array $options = []): PocketAction {
    if ($id_or_url instanceof Url) {
      return (new static('add', NULL, $options))
        ->set('url', $id_or_url->setAbsolute()->toString());
    }
    if (is_numeric($id_or_url)) {
      return new static('add', (int) $id_or_url, $options);
    }
    throw new \InvalidArgumentException('Argument is not an integer or a Url object.');
  }

  /**
   * Archive an item, moving it from unread to archive.
   *
   * @param int $id
   *   The item to be archived.
   * @param array $options
   *   (Optional) Any additional parameters.
   *
   * @return static
   *
   * @see https://getpocket.com/developer/docs/v3/modify#action_archive
   */
  public static function archive(int $id, array $options = []): PocketAction {
    return new static('archive', $id, $options);
  }

  /**
   * Readd an item, moving it from archive back to unread.
   *
   * @param int $id
   *   The item to be readded.
   * @param array $options
   *   (Optional) Any additional parameters.
   *
   * @return static
   *
   * @see https://getpocket.com/developer/docs/v3/modify#action_readd
   */
  public static function readd(int $id, array $options = []): PocketAction {
    return new static('readd', $id, $options);
  }

  /**
   * Favorite an item.
   *
   * @param int $id
   *   The item to be favorited.
   * @param array $options
   *   (Optional) Any additional parameters.
   *
   * @return static
   *
   * @see https://getpocket.com/developer/docs/v3/modify#action_favorite
   */
  public static function favorite(int $id, array $options = []): PocketAction {
    return new static('favorite', $id, $options);
  }

  /**
   * Unfavorite an item.
   *
   * @param int $id
   *   The item to be unfavorited.
   * @param array $options
   *   (Optional) Any additional parameters.
   *
   * @return static
   *
   * @see https://getpocket.com/developer/docs/v3/modify#action_unfavorite
   */
  public static function unfavorite(int $id, array $options = []): PocketAction {
    return new static('unfavorite', $id, $options);
  }

  /**
   * Delete an item.
   *
   * @param int $id
   *   The item to be deleted.
   * @param array $options
   *   (Optional) Any additional parameters.
   *
   * @return static
   *
   * @see https://getpocket.com/developer/docs/v3/modify#action_delete
   */
  public static function delete(int $id, array $options = []): self {
    return new static('delete', $id, $options);
  }

  /**
   * Add tags to an item.
   *
   * @param int $id
   *   The item to be tagged.
   * @param string[] $tags
   *   The tags to add. Note: Tag names containing commas will be split.
   * @param array $options
   *   (Optional) Any additional parameters.
   *
   * @return static
   *
   * @see https://getpocket.com/developer/docs/v3/modify#action_tags_add
   */
  public static function tagsAdd(int $id, array $tags, array $options = []): self {
    return (new static('tags_add', $id, $options))
      ->set('tags', implode(',', $tags));
  }

  /**
   * Remove tags from an item.
   *
   * @param int $id
   *   The item to be untagged.
   * @param string[] $tags
   *   The tags to remove. Note: Tag names containing commas will be split.
   * @param array $options
   *   (Optional) Any additional parameters.
   *
   * @return static
   *
   * @see https://getpocket.com/developer/docs/v3/modify#action_tags_remove
   */
  public static function tagsRemove(int $id, array $tags, array $options = []): self {
    return (new static('tags_remove', $id, $options))
      ->set('tags', implode(',', $tags));
  }

  /**
   * Add tags to an item after removing all existing tags.
   *
   * @param int $id
   *   The item to be tagged.
   * @param string[] $tags
   *   The tags to add. Note: Tag names containing commas will be split.
   * @param array $options
   *   (Optional) Any additional parameters.
   *
   * @return static
   *
   * @see https://getpocket.com/developer/docs/v3/modify#action_tags_replace
   */
  public static function tagsReplace(int $id, array $tags, array $options = []): self {
    return (new static('tags_replace', $id, $options))
      ->set('tags', implode(',', $tags));
  }

  /**
   * Remove all existing tags from an item.
   *
   * @param int $id
   *   The item to be untagged.
   * @param array $options
   *   (Optional) Any additional parameters.
   *
   * @return static
   *
   * @see https://getpocket.com/developer/docs/v3/modify#action_tags_clear
   */
  public static function tagsClear(int $id, array $options = []): self {
    return new static('tags_remove', $id, $options);
  }

  /**
   * Rename a tag.
   *
   * @param string $old
   *   Old tag name.
   * @param string $new
   *   New tag name.
   * @param array $options
   *   Any additional parameters.
   *
   * @return static
   *
   * @see https://getpocket.com/developer/docs/v3/modify#action_tag_rename
   */
  public static function tagRename(string $old, string $new, array $options = []): PocketAction {
    return (new static('tag_rename', NULL, $options))
      ->set('old_tag', $old)
      ->set('new_tag', $new);
  }

  /**
   * Delete a tag.
   *
   * @param string $tag
   *   Tag name.
   * @param array $options
   *   Any additional parameters.
   *
   * @return static
   *
   * @see https://getpocket.com/developer/docs/v3/modify#action_tag_rename
   */
  public static function tagDelete(string $tag, array $options = []): PocketAction {
    return (new static('tag_rename', NULL, $options))
      ->set('tag', $tag);
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $name): mixed {
    return $this->values[$name] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function set(string $name, mixed $value = NULL): self {
    $this->values[$name] = $value;
    if ($value === NULL) {
      unset($this->values[$name]);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function serialize(): array {
    return $this->values;
  }

  /**
   * {@inheritdoc}
   */
  public function isSuccessful(): bool {
    return $this->result;
  }

  /**
   * {@inheritdoc}
   */
  public function setResult(bool $result): self {
    $this->result = $result;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getResultItem(): ?PocketItemInterface {
    return $this->item;
  }

  /**
   * {@inheritdoc}
   */
  public function setResultItem(PocketItemInterface $item): self {
    $this->item = $item;
    return $this;
  }

  /**
   * Set the item ID.
   *
   * @param string $id
   *   The ID.
   *
   * @return $this
   */
  public function setId(string $id): self {
    return $this->set('item_id', $id);
  }

}
