<?php

namespace Drupal\pocket;

/**
 * Defines a data object representing a Pocket item.
 *
 * @package Drupal\pocket
 */
interface PocketItemInterface {

  /**
   * Unique ID.
   *
   * @return string
   *   The unique ID.
   */
  public function id(): string;

  /**
   * Item URL.
   *
   * @return string
   *   The URL.
   */
  public function getUrl(): string;

  /**
   * Unique domain ID.
   *
   * @return string
   *   The unique domain ID.
   */
  public function getDomainId(): string;

  /**
   * Resolved ID.
   *
   * @return string
   *   The resolved ID.
   */
  public function getResolvedId(): string;

  /**
   * Resolved item URL (after redirects).
   *
   * @return string
   *   The resolved URL.
   */
  public function getResolvedUrl(): string;

  /**
   * Resolved domain ID.
   *
   * @return string
   *   The resolved domain ID.
   */
  public function getResolvedDomainId(): string;

  /**
   * HTTP response code.
   *
   * @return int
   *   The HTTP response code.
   */
  public function getResponseCode(): int;

  /**
   * MIME type of content.
   *
   * @return string
   *   The MIME type.
   */
  public function getMimeType(): string;

  /**
   * Byte length of content.
   *
   * @return int
   *   The length in bytes.
   */
  public function getContentLength(): int;

  /**
   * Encoding of content.
   *
   * @return string
   *   The character encoding of the content.
   */
  public function getEncoding(): string;

  /**
   * Date the item was added.
   *
   * @return \DateTime
   *   The added timestamp.
   */
  public function getAddedDate(): \DateTime;

  /**
   * Published date of the item (if discovered).
   *
   * @return \DateTime|null
   *   The published timestamp, or NULL.
   */
  public function getPublishedDate(): ?\DateTime;

  /**
   * Content title.
   *
   * @return string
   *   The content title.
   */
  public function getTitle(): string;

  /**
   * Content excerpt.
   *
   * @return string
   *   The content excerpt.
   */
  public function getExcerpt(): string;

  /**
   * Content word count.
   *
   * @return int
   *   The content word count.
   */
  public function getWordCount(): int;

  /**
   * Check if the item has at least one image.
   *
   * @return bool
   *   TRUE if the item has at least one image.
   */
  public function hasImage(): bool;

  /**
   * Check if the item has at least one video.
   *
   * @return bool
   *   TRUE if the item has at least one video.
   */
  public function hasVideo(): bool;

  /**
   * Check if the item is an article.
   *
   * @return bool
   *   TRUE if the item is an article.
   */
  public function isArticle(): bool;

  /**
   * Check if the item is an index page.
   *
   * @return bool
   *   TRUE if the item is an index page.
   */
  public function isIndex(): bool;

  /**
   * Get all authors of the item.
   *
   * @return array[]
   *   Sequence of arrays, each containing the following keys:
   *   - item_id: The parent item ID.
   *   - author_id: The author ID.
   *   - name: The author name.
   *   - url: The author URL, or an empty string.
   */
  public function getAuthors(): array;

  /**
   * Get all images from the item.
   *
   * @return array[]
   *   Sequence of arrays, each containing the following keys:
   *   - item_id: The parent item ID.
   *   - image_id: Sequential ID of the image within the item.
   *   - src: The URL of the image.
   *   - width: Pixel width.
   *   - height: Pixel height.
   *   - credit: Image attribution.
   *   - caption: Image caption.
   */
  public function getImages(): array;

  /**
   * Get all videos from the item.
   *
   * @return array
   *   A sequence of arrays.
   */
  public function getVideos(): array;

  /**
   * Get any item value.
   *
   * @param string $key
   *   The key.
   *
   * @return mixed
   *   The value, or NULL if it does not exist.
   */
  public function get(string $key): mixed;

}
