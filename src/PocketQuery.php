<?php

namespace Drupal\pocket;

use Drupal\pocket\Client\PocketUserClientInterface;

/**
 * A query builder for Pocket.
 */
class PocketQuery implements PocketQueryInterface {

  public const STATE_ALL = 'all';

  public const STATE_ARCHIVED = 'archived';

  public const STATE_UNREAD = 'unread';

  public const ORDER_NEWEST = 'newest';

  public const ORDER_OLDEST = 'oldest';

  public const ORDER_SITE = 'site';

  public const ORDER_TITLE = 'title';

  public const TYPE_ARTICLE = 'article';

  public const TYPE_IMAGE = 'image';

  public const TYPE_VIDEO = 'video';

  /**
   * The user client to send this query to.
   *
   * @var \Drupal\pocket\Client\PocketUserClientInterface
   */
  protected PocketUserClientInterface $client;

  /**
   * Retrieve favorited items.
   *
   * @var bool
   */
  protected bool $favorites;

  /**
   * Retrieve non-favorited items.
   *
   * @var bool
   */
  protected bool $nonFavorites;

  /**
   * Query data.
   *
   * @var array
   */
  protected array $values;

  /**
   * PocketQuery constructor.
   *
   * @param \Drupal\pocket\Client\PocketUserClientInterface|null $client
   *   (Optional) The Pocket user client.
   *   Can also be set when calling ::execute() instead.
   * @param array $values
   *   The query values.
   */
  public function __construct(PocketUserClientInterface $client = NULL, array $values = []) {
    $this->client = $client;
    $this->values = $values;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(PocketUserClientInterface $client = NULL): array {
    $client = $client ?? $this->client;
    if ($client === NULL) {
      throw new \InvalidArgumentException('No client to send the query to.');
    }
    return $client->get($this->buildQuery());
  }

  /**
   * {@inheritdoc}
   */
  public function getState(string $state): self {
    $this->values['state'] = $state;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFavorite(bool $favorite = NULL): self {
    $this->values['favorite'] = $favorite;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getContentType(string $type = NULL): self {
    $this->values['contentType'] = $type;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTag(string $tag = NULL): self {
    $this->values['tag'] = $tag !== '' ? $tag : '_untagged_';
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrder(string $order): self {
    $this->values['sort'] = $order;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDetails(bool $details = TRUE): self {
    $this->values['detailType'] = $details ? 'complete' : 'simple';
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function search(string $search = NULL): self {
    $this->values['search'] = $search;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomain(string $domain = NULL): self {
    $this->values['domain'] = $domain;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSince(int $timestamp = NULL): self {
    $this->values['since'] = $timestamp;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRange(int $offset = NULL, int $count = NULL): self {
    $this->values['offset'] = $count ? $offset : NULL;
    $this->values['count'] = $count;
    return $this;
  }

  /**
   * Build the query as an array of parameters.
   *
   * @return array
   *   The data to send to the endpoint.
   */
  protected function buildQuery(): array {
    foreach ($this->values as $key => $value) {
      if ($value === NULL) {
        unset($this->values[$key]);
      }
    }
    return $this->values;
  }

}
