<?php

namespace Drupal\pocket;

/**
 * Data object for an access token, storing username and token identifier.
 */
class AccessToken {

  /**
   * The actual access token string.
   *
   * @var string
   */
  private string $token;

  /**
   * The Pocket username associated with the token.
   *
   * @var string
   */
  private string $username;

  /**
   * State data associated with the token.
   *
   * @var array
   */
  private array $state;

  /**
   * AccessToken constructor.
   *
   * @param string $token
   *   The token string.
   * @param string $username
   *   The Pocket username.
   */
  public function __construct(string $token, string $username) {
    $this->token = $token;
    $this->username = $username;
  }

  /**
   * Get the token string.
   *
   * @return string
   *   The token string.
   */
  public function getToken(): string {
    return $this->token;
  }

  /**
   * Get the Pocket username.
   *
   * @return string
   *   The Pocket username.
   */
  public function getUsername(): string {
    return $this->username;
  }

  /**
   * Set the state data.
   *
   * @param array $state
   *   The state data.
   *
   * @return $this
   */
  public function setState(array $state): AccessToken {
    $this->state = $state;
    return $this;
  }

  /**
   * Get the state data.
   *
   * @return array
   *   The state data.
   */
  public function getState(): array {
    return $this->state;
  }

}
