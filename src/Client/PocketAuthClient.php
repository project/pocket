<?php

namespace Drupal\pocket\Client;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Url;
use Drupal\pocket\AccessToken;
use GuzzleHttp\ClientInterface;

/**
 * An OAuth client that authorizes the application to act for a user.
 */
class PocketAuthClient extends PocketClient implements PocketAuthClientInterface {

  /**
   * The key-value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected KeyValueStoreInterface $storage;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected UuidInterface $uuid;

  /**
   * PocketAuthClient constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http
   *   The HTTP client service.
   * @param string $consumer_key
   *   The API consumer key.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreInterface $storage
   *   The key-value store.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID service.
   */
  public function __construct(
    ClientInterface $http,
    string $consumer_key,
    KeyValueStoreInterface $storage,
    UuidInterface $uuid
  ) {
    parent::__construct($http, $consumer_key);
    $this->storage = $storage;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function authorize(callable $callback, array $state = []): Url {
    $id = $this->uuid->generate();
    $redirect = new Url('pocket.authorize', ['id' => $id]);
    $token = $this->getRequestToken($redirect, $id);
    $this->storage->set(
      "request:$id",
      [
        'token'    => $token,
        'callback' => $callback,
        'state'    => $state,
      ]
    );

    return Url::fromUri(
      static::URL . 'auth/authorize',
      [
        'query' => [
          'request_token' => $token,
          'redirect_uri'  => $redirect->setAbsolute()->toString(),
        ],
      ]
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Drupal\pocket\Exception\PocketHttpException
   */
  public function getRequestToken(Url $redirect, string $state = NULL): string {
    $request = ['redirect_uri' => $redirect->setAbsolute()->toString()];
    if ($state) {
      $request['state'] = $state;
    }
    $response = $this->sendRequest('v3/oauth/request', $request);
    return $response['code'];
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken(string $requestToken): AccessToken {
    $request = ['code' => $requestToken];
    $response = $this->sendRequest('v3/oauth/authorize', $request);
    return new AccessToken($response['access_token'], $response['username']);
  }

}
