<?php

namespace Drupal\pocket\Client;

/**
 * Interface for the client factory.
 */
interface PocketClientFactoryInterface {

  /**
   * Create an authorization client.
   *
   * @return \Drupal\pocket\Client\PocketAuthClientInterface
   *   The auth client.
   */
  public function getAuthClient(): PocketAuthClientInterface;

  /**
   * Create a user client for a specific user.
   *
   * The user must already have authorized the application.
   *
   * @param string $accessToken
   *   The authorized access token.
   *
   * @return \Drupal\pocket\Client\PocketUserClientInterface
   *   The user client.
   */
  public function getUserClient(string $accessToken): PocketUserClientInterface;

  /**
   * Check if the API key exists.
   *
   * @return bool
   *   TRUE if the application has an API consumer key.
   */
  public function hasKey(): bool;

  /**
   * Get the API key.
   *
   * @return string|null
   *   The API consumer key.
   */
  public function getKey(): ?string;

}
