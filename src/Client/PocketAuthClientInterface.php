<?php

namespace Drupal\pocket\Client;

use Drupal\Core\Url;
use Drupal\pocket\AccessToken;

/**
 * Defines the OAuth client for authorizing a Pocket application.
 *
 * @package Drupal\pocket\Client
 */
interface PocketAuthClientInterface {

  /**
   * Generate a request token and automatically process the redirect.
   *
   * Note: Callback and state data must be serializable.
   *
   * @param callable $callback
   *   The callback to run on the finished access token.
   *   This callback will receive an AccessToken object and must return
   *   a URL for the landing page.
   * @param array $state
   *   Optional state information to store with the request.
   *
   * @return \Drupal\Core\Url
   *   Returns the Pocket URL to which to redirect the user for authorization.
   *
   * @throws \Drupal\pocket\Exception\PocketHttpException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function authorize(callable $callback, array $state = []): Url;

  /**
   * Generate a request token that redirects to a specific URL.
   *
   * @param \Drupal\Core\Url $redirect
   *   A URL to redirect to after authorization.
   * @param string|null $state
   *   An optional state string to identify responses.
   *
   * @return string
   *   The request token.
   */
  public function getRequestToken(Url $redirect, string $state = NULL): string;

  /**
   * Turn a request token into an access token.
   *
   * The access token also contains the username it is associated with.
   *
   * @param string $requestToken
   *   The request token.
   *
   * @return \Drupal\pocket\AccessToken
   *   The access token.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Drupal\pocket\Exception\PocketHttpException
   */
  public function getAccessToken(string $requestToken): AccessToken;

}
