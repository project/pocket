<?php

namespace Drupal\pocket\Client;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\pocket\Exception\PocketConfigurationException;
use GuzzleHttp\ClientInterface;

/**
 * Client factory class.
 */
class PocketClientFactory implements PocketClientFactoryInterface {

  /**
   * The HTTP client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $client;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The key/value factory service.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface
   */
  protected KeyValueExpirableFactoryInterface $storageFactory;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected UuidInterface $uuid;

  /**
   * The API consumer key.
   *
   * @var string
   */
  private string $key;

  /**
   * PocketClientFactory constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $storageFactory
   *   The key-value factory service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID service.
   */
  public function __construct(
    ClientInterface $client,
    ConfigFactoryInterface $configFactory,
    KeyValueExpirableFactoryInterface $storageFactory,
    UuidInterface $uuid
  ) {
    $this->client = $client;
    $this->configFactory = $configFactory;
    $this->storageFactory = $storageFactory;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\pocket\Exception\PocketConfigurationException
   *   If no API consumer key is set.
   */
  public function getAuthClient(): PocketAuthClient {
    if (!$this->hasKey()) {
      throw new PocketConfigurationException('Cannot create client without an API consumer key.');
    }
    return new PocketAuthClient(
      $this->client,
      $this->getKey(),
      $this->storageFactory->get('pocket'),
      $this->uuid
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\pocket\Exception\PocketConfigurationException
   *   If no API consumer key is set.
   */
  public function getUserClient(string $accessToken): PocketUserClientInterface {
    if (!$this->hasKey()) {
      throw new PocketConfigurationException('Cannot create client without an API consumer key.');
    }
    return new PocketUserClient($this->client, $this->getKey(), $accessToken);
  }

  /**
   * {@inheritdoc}
   */
  public function hasKey(): bool {
    return $this->getKey() !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getKey(): ?string {
    if (!$this->key) {
      $this->key = $this->configFactory->get('pocket.config')->get('key');
    }
    return $this->key;
  }

}
