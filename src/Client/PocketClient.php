<?php

namespace Drupal\pocket\Client;

use Drupal\Component\Serialization\Json;
use Drupal\pocket\Exception\PocketHttpException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;

/**
 * A Pocket application client.
 *
 * Sends and receives JSON data to and from the Pocket endpoints.
 * This class is designed to be extended by the actual client classes.
 */
class PocketClient {

  /**
   * The main service URL.
   */
  public const URL = 'https://getpocket.com/';

  /**
   * The HTTP client that sends requests.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $http;

  /**
   * The consumer key that identifies the application to Pocket.
   *
   * @var string
   */
  private string $consumerKey;

  /**
   * PocketClient constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http
   *   The HTTP client.
   * @param string $consumer_key
   *   The consumer key.
   */
  public function __construct(ClientInterface $http, string $consumer_key) {
    $this->http = $http;
    $this->consumerKey = $consumer_key;
  }

  /**
   * Send a request to a Pocket API endpoint.
   *
   * @param string $endpoint
   *   The API endpoint.
   * @param array $request
   *   The data to send.
   *
   * @return array
   *   The response data.
   *
   * @throws \Drupal\pocket\Exception\PocketHttpException
   *   If Pocket returned a known error.
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   If another HTTP error occurred.
   */
  protected function sendRequest(string $endpoint, array $request): array {
    $request['consumer_key'] = $this->consumerKey;
    return $this->sendJson(static::URL . $endpoint, $request);
  }

  /**
   * Send JSON data to a URL.
   *
   * @param string $url
   *   The URL.
   * @param array $body
   *   The request body.
   *
   * @return array
   *   The response data.
   *
   * @throws \Drupal\pocket\Exception\PocketHttpException
   *   If Pocket returned a known error.
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   If another HTTP error occurred.
   */
  protected function sendJson(string $url, array $body): array {
    try {
      $response = $this->http->request('POST', $url, [
        'json' => $body,
        'headers' => ['X-Accept' => 'application/json'],
      ]);
      try {
        $data = $response->getBody()->getContents();
      }
      catch (\RuntimeException $e) {
        watchdog_exception('pocket', $e);
        $data = '';
      }
      return Json::decode($data);
    }
    catch (BadResponseException $e) {
      throw PocketHttpException::create($e);
    }
  }

}
