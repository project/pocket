<?php

namespace Drupal\pocket\Client;

use Drupal\Core\Url;
use Drupal\pocket\PocketItemInterface;
use Drupal\pocket\PocketQueryInterface;

/**
 * Pocket client interface.
 */
interface PocketUserClientInterface {

  /**
   * Perform a request on 'v3/add'.
   *
   * This adds a single item to Pocket. Use ::send([PocketAction::add(), ...])
   * to add multiple items at once.
   *
   * @param \Drupal\Core\Url $url
   *   URL of the submitted content.
   * @param string[] $tags
   *   (Optional) list of tags.
   * @param string|null $title
   *   (Optional) title. Ignored if the URL provides its own title.
   *
   * @return \Drupal\pocket\PocketItemInterface
   *   The item metadata returned by Pocket.
   *
   * @see https://getpocket.com/developer/docs/v3/add
   */
  public function add(Url $url, array $tags = [], string $title = NULL): PocketItemInterface;

  /**
   * Perform a request on 'v3/send'.
   *
   * Success can be checked for each action via ::isSuccessful().
   *
   * @param \Drupal\pocket\Action\PocketActionInterface[] $actions
   *   An array of actions.
   *
   * @return \Drupal\pocket\Action\PocketActionInterface[]
   *   The same array. Use $action->isSuccessful() and $action->getResultItem()
   *   to get the result of each action.
   *
   * @see https://getpocket.com/developer/docs/v3/modify
   */
  public function send(array $actions): array;

  /**
   * Perform a request on 'v3/get'.
   *
   * @param array $query
   *   An array of query parameters.
   *
   * @return \Drupal\pocket\PocketItemInterface[]
   *   An array of Pocket items.
   *
   * @see https://getpocket.com/developer/docs/v3/retrieve
   */
  public function get(array $query): array;

  /**
   * Build a query object for retrieving items.
   *
   * @param array $options
   *   An array of options.
   *
   * @return \Drupal\pocket\PocketQueryInterface
   *   A Pocket query.
   */
  public function query(array $options = []): PocketQueryInterface;

}
