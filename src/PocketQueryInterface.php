<?php

namespace Drupal\pocket;

use Drupal\pocket\Client\PocketUserClientInterface;

/**
 * Defines the interface for pocket queries.
 *
 * Builds a query compliant with the Pocket /v3/retrieve endpoint.
 *
 * @see https://getpocket.com/developer/docs/v3/retrieve
 */
interface PocketQueryInterface {

  /**
   * Runs the query.
   *
   * @param \Drupal\pocket\Client\PocketUserClientInterface|null $client
   *   (Optional) Client to send the query to. Will override any client already
   *   set, but only for this call.
   *
   * @return \Drupal\pocket\PocketItemInterface[]
   *   The result set as an array of Pocket items.
   */
  public function execute(PocketUserClientInterface $client = NULL): array;

  /**
   * Set the state parameter.
   *
   * @param string $state
   *   The state parameter. Must be "unread", "archive", or "all".
   *
   * @return $this
   */
  public function getState(string $state): self;

  /**
   * Include only favorites in the result.
   *
   * @param bool|null $favorite
   *   TRUE to get favorites, FALSE to get non-favorites, NULL to unset.
   *
   * @return $this
   */
  public function getFavorite(bool $favorite = NULL): self;

  /**
   * Filter by a specific type.
   *
   * @param string|null $type
   *   The type. Must be "article", "video", "image", or NULL to unset.
   *
   * @return $this
   */
  public function getContentType(string $type = NULL): self;

  /**
   * Filter by a specific tag.
   *
   * @param string|null $tag
   *   The tag. Must be either a tag name or "" for untagged items.
   *
   * @return $this
   */
  public function getTag(string $tag = NULL): self;

  /**
   * Set the sort order for the results.
   *
   * @param string $order
   *   The order parameter. Must be "newest", "oldest", "title" or "site".
   *
   * @return $this
   */
  public function setOrder(string $order): self;

  /**
   * Include extended details in the result.
   *
   * Basic information includes title, URL, status, etc.
   *
   * Extended information includes tags, images, authors, videos, etc.
   *
   * @param bool $details
   *   TRUE to include extended details.
   *
   * @return $this
   */
  public function getDetails(bool $details = TRUE): self;

  /**
   * Filter items by text search in the title and URL.
   *
   * @param string|null $search
   *   The search string, or NULL to unset.
   *
   * @return $this
   */
  public function search(string $search = NULL): self;

  /**
   * Get items from a specific domain.
   *
   * @param string|null $domain
   *   The domain, or NULL to unset.
   *
   * @return $this
   */
  public function getDomain(string $domain = NULL): self;

  /**
   * Get items created after a certain time.
   *
   * @param int|null $timestamp
   *   The timestamp, or NULL to unset.
   *
   * @return $this
   */
  public function getSince(int $timestamp = NULL): self;

  /**
   * Get a specific range of items.
   *
   * Offset only takes effect when count is set, and defaults to zero.
   *
   * @param int|null $offset
   *   The offset of the first item to retrieve.
   * @param int|null $count
   *   The number of items to retrieve.
   *
   * @return $this
   */
  public function getRange(int $offset = NULL, int $count = NULL): self;

}
