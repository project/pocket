<?php

namespace Drupal\pocket\Exception;

/**
 * Error condition triggered by using the module in a badly configured state.
 */
class PocketConfigurationException extends \Exception {}
