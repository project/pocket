<?php

namespace Drupal\pocket\Exception;

use GuzzleHttp\Exception\BadResponseException;
use Psr\Http\Message\ResponseInterface;

/**
 * Wrapper exception for HTTP errors on Pocket.
 */
class PocketHttpException extends \Exception {

  /**
   * The original HTTP exception.
   *
   * @var \GuzzleHttp\Exception\BadResponseException
   */
  protected BadResponseException $original;

  /**
   * The HTTP response.
   *
   * @var \Psr\Http\Message\ResponseInterface
   */
  protected ResponseInterface $response;

  /**
   * PocketHttpException constructor.
   *
   * Use ::create() to produce the appropriate sub-type based on the response.
   *
   * @param \GuzzleHttp\Exception\BadResponseException $exception
   *   The original HTTP exception.
   */
  public function __construct(BadResponseException $exception) {
    $response = $exception->getResponse();
    $header = $response ? $response->getHeader('X-Error') : [];
    parent::__construct($header[0] ?? $exception->getMessage());
    $this->original = $exception;
    $this->response = $response;
  }

  /**
   * Checks the HTTP response code and creates the appropriate subtype.
   *
   * @param \GuzzleHttp\Exception\BadResponseException $exception
   *   The original HTTP exception.
   *
   * @return static
   */
  public static function create(BadResponseException $exception): self {
    return match ($exception->getCode()) {
      403 => new AccessDeniedException($exception),
      401 => new UnauthorizedException($exception),
      default => new static($exception),
    };

  }

  /**
   * Get a named header from the HTTP response.
   *
   * @param string $name
   *   The header name.
   *
   * @return string[]
   *   All header values with this name.
   */
  public function getHeader(string $name): array {
    return $this->response->getHeader($name);
  }

}
