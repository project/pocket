<?php

namespace Drupal\pocket\Exception;

/**
 * Indicates a 403 error.
 */
class AccessDeniedException extends PocketHttpException {}
