<?php

namespace Drupal\pocket\Exception;

/**
 * Indicates a 401 error.
 */
class UnauthorizedException extends PocketHttpException {}
