<?php

/**
 * @file
 * Sample code for using the Pocket module.
 */

use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\pocket\AccessToken;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @defgroup pocket_api Pocket API
 * @{
 * Controller function that redirects the user to Pocket.
 *
 * In the OAuth workflow, the application (Drupal) begins by asking the service
 * (Pocket) to generate a new request token. Along with this request, the
 * application sends a URI and a state identifier that is stored by the service.
 *
 * The application then redirects the user to the service, where they will
 * authenticate themselves and authorize the application to act on their behalf.
 *
 * Once they confirm, the service then redirects the user back to the
 * application, using the client URI and state identifier saved earlier.
 *
 * The short-lived request token having now been authorized, the application
 * asks the service for a longer-lived access token, which will remain active
 * until the user revokes their authorization.
 *
 * @return \Symfony\Component\HttpFoundation\RedirectResponse
 *   The redirect response that will send the user to Pocket.
 *
 * @throws \Drupal\pocket\Exception\PocketHttpException
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function _pocket_example_authorize(): RedirectResponse {
  /** @var \Drupal\pocket\Client\PocketClientFactoryInterface $factory */
  $factory = Drupal::service('pocket.client');
  $user = Drupal::currentUser()->id();
  return new TrustedRedirectResponse(
    $factory->getAuthClient()->authorize(
      '_pocket_example_callback',
      ['user' => $user]
    )->toString()
  );
}

/**
 * Callback function that stores the access token and returns to the user page.
 *
 * This function will be called by the Pocket auth client as soon as the user
 * is redirected back to the application after authorizing the application.
 *
 * The callback is expected to return a URL that the user is supposed to
 * be redirected to after completion - this is generally the same URL they
 * were on before the OAuth process began.
 *
 * @param \Drupal\pocket\AccessToken $access
 *   The access token object. This is a data object containing the Pocket
 *   username and the actual access token string.
 *
 * @return \Drupal\Core\Url
 *   The URL to redirect the user to after completion.
 *
 * @throws \Drupal\Core\Entity\EntityMalformedException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function _pocket_example_callback(AccessToken $access): Url {
  $id = $access->getState()['user'];
  /** @var \Drupal\user\Entity\User $user */
  $user = User::load($id);

  $user->set('pocket_username', $access->getUsername())
    ->set('pocket_access', $access->getToken())
    ->save();
  Drupal::messenger()->addStatus('You have successfully connected your "%user" Pocket account.', [
    '%user' => $user->pocket_username,
  ]);
  return $user->toUrl();
}

/**
 * Connect to the Pocket service as a specific user, and push a page.
 *
 * @param \Drupal\node\NodeInterface $node
 *   A Drupal node object.
 * @param \Drupal\user\UserInterface $user
 *   A Drupal user object.
 *
 * @throws \Drupal\Core\Entity\EntityMalformedException
 */
function _pocket_example_add_node(NodeInterface $node, UserInterface $user) {
  /** @var \Drupal\pocket\Client\PocketClientFactoryInterface $factory */
  $factory = Drupal::service('pocket.client');
  $factory->getUserClient($user->pocket_access)
    ->add($node->toUrl());
}

/**
 * @}
 */
