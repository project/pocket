INTRODUCTION
------------

The Pocket module is a client that can interact with the web API of the
[Pocket](https://getpocket.com/) service. A Drupal site can use this to
automatically add, modify or retrieve items on a user's Pocket account.

REQUIREMENTS
------------

Drupal 8.5.0 or later.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/895232/ for further information.

CONFIGURATION
-------------

 * Create a new application on https://getpocket.com/developer/ to get a key.
 * Enter the key at Administration » Configuration » Web services » Pocket.

EXTENDING
---------

This module currently provides no end-user functionality. The "Pocket User"
submodule allows Drupal users to connect a Pocket account, but there is no
built-in functionality for using that connection yet.
